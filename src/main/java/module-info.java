module cz.cvut.fit.angrybirds {
    requires javafx.controls;
    requires java.desktop;

    exports cz.cvut.fit.angrybirds;
}