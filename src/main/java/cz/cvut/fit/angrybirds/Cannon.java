package cz.cvut.fit.angrybirds;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import java.io.File;

public class Cannon extends GameObject {

    private static final int IMAGE_HEIGHT = 69;
    private int angle = 0;

    Cannon() {
        texture = "src/main/resources/cannon.png";
        height = 69;
        width = 25;
    }

    public void moveUp() {
        if (getY() > 0) {
            this.move(0, -1 * GameConfig.MOVE_STEP);
        }
    }

    public void moveDown() {
        if (getY() + IMAGE_HEIGHT < GameConfig.MAX_Y) {
            this.move(0, GameConfig.MOVE_STEP);
        }
    }

    public Missile shoot() {
        Missile missile = new Missile();
        //calculate velocity of missile on both axis
        missile.setAngle(this.getAngle());

        missile.initVelocities(GameConfig.VELOCITY);
        missile.setVelocityY(this.getAngle());
        missile.setPosition(getPosition());

        playAudio();

        return missile;
    }

    private void playAudio() {
        try {
            AudioInputStream audioInputStream = AudioSystem
                    .getAudioInputStream(new File("src/main/resources/cannonBlast.aiff").getAbsoluteFile());
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);

            clip.start();

            clip.addLineListener(event -> {
                if (event.getType().equals(LineEvent.Type.STOP)) {
                    clip.close();
                }
            });

            audioInputStream.close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public int getAngle() {
        return angle;
    }

    public void setAngle(int angle) {
        this.angle = angle;
    }

    public void increaseAngle() {
        if (angle < 60) {
            angle++;
        }
    }

    public void decreaseAngle() {
        if (angle > -60) {
            angle--;
        }
    }
}
