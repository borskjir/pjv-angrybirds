package cz.cvut.fit.angrybirds;

public abstract class GameObject {
    private Position pos = new Position(0, 0);
    protected String texture;

    protected int height;
    protected int width;

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    /**
     * !!! WARNING !!!
     * Function expects only object of the same size and object can't be too thin.
     * This method will be malfunctioning if movement is too fast.
     * @param go
     * @return true if object collides
     */
    public boolean collideWith(final GameObject go) {
        // right corner
        if(go.getX() <= this.getX() + this.getWidth() &&
                this.getX() + this.getWidth() <= go.getX() + go.getWidth()) {

            // right top corner
            if(go.getY() <= this.getY() &&
                    this.getY() <= go.getY() + go.getWidth()){

                return true;
            }

            // right left corner
            if(go.getY() <= this.getY()+this.getHeight() &&
                    this.getY()+this.getHeight() <= go.getY() + go.getWidth()){

                return true;
            }
        }
        return false;
    }

    public String getTexture() {
        return texture;
    }

    public Position getPosition() {
        return new Position(this.pos);
    }

    public void setPosition(final Position position) {
        this.pos = new Position(position);
    }

    public double getX() {
        return this.pos.getX();
    }

    public double getY() {
        return this.pos.getY();
    }

    public void setX(double x) {
        this.pos.setX(x);
    }

    public void setY(double y) {
        this.pos.setY(y);
    }

    public void move(double dx, double dy) {
        this.pos.move(dx, dy);
    }
}
