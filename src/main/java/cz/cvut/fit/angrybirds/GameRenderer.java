package cz.cvut.fit.angrybirds;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.io.File;

public class GameRenderer {

    private GraphicsContext graphicsContext;

    public GameRenderer(GraphicsContext graphicsContext) {
        this.graphicsContext = graphicsContext;
    }

    public void drawImage(String path, Position pos) {
        graphicsContext.drawImage(new Image(new File(path).toURI().toString()), pos.getX(), pos.getY());
    }

    public void drawText(String text, Position pos) {
        graphicsContext.strokeText(text, pos.getX(), pos.getY());
    }

    public void clearRect(int i, int j, int maxX, int maxY) {
        graphicsContext.clearRect(i, j, maxX, maxY);
    }

}
