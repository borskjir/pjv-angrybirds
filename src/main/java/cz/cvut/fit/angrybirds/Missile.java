package cz.cvut.fit.angrybirds;

public class Missile extends GameObject{

    Missile(){
        texture = "src/main/resources/missile.png";
        height = 29;
        width = 30;
    }

    public void updatePosition() {
        double timeInc = GameConfig.REAL_MOVE_TIME_INC;

        setTime(getTime() + timeInc);
        move((getVelocityX() * timeInc), - (getVelocityY() * timeInc));
        setVelocityY(getVelocityY() - getGravity() * timeInc);
    }

    protected double velocityX = GameConfig.MOVE_STEP;
    protected double velocityY = 0;

    protected double angle = 0;
    protected double time = 0.0;
    private double gravity = 5;  // TODO add gravity setting

    public double getGravity() {
        return gravity;
    }

    public double setGravity(double gravity) {
        return this.gravity = gravity;
    }

    public double getTime(){
        return this.time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    private double getAngle() {
        return this.angle;
    }

    public void initVelocities(double velocity){
        velocityX = velocity * Math.cos(Math.PI / 180 * this.getAngle());
        velocityY = velocity * Math.sin(Math.PI / 180 * this.getAngle());
    }

    public double getVelocityX() {
        return velocityX;
    }

    public double getVelocityY() {
        return velocityY;
    }

    public void setVelocityY(double velocityY) {
        this.velocityY = velocityY;
    }
}
