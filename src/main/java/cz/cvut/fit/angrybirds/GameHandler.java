package cz.cvut.fit.angrybirds;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GameHandler {

    private Cannon cannon = new Cannon();
    private List<Enemy> enemies = new ArrayList<>();
    private List<Missile> missiles = new ArrayList<>();

    private int score = 0;

    private GameRenderer renderer;

    public void init() {
        for (int i = 0; i < GameConfig.ENEMIES_COUNT; ++i) {
            enemies.add(new Enemy());
        }
    }

    public void setGameRenderer(GameRenderer gameRenderer) {
        renderer = gameRenderer;
    }

    public void processPressedKeys(Map<String, Boolean> pressedKeysCodes) {
        for(Map.Entry<String, Boolean> entry: pressedKeysCodes.entrySet()) {
            if(!entry.getValue()) {
                entry.setValue(handleKeyCode(entry.getKey()));
            }
        }
    }

    private boolean handleKeyCode(String keyCode) {
        switch (keyCode) {
            case "UP": // move cannon up
                cannon.moveUp();
                break;
            case "DOWN": // move cannon down
                cannon.moveDown();
                break;
            case "LEFT": // change cannon angle toward sky
                cannon.increaseAngle();
                break;
            case "RIGHT": // change cannon angle toward ground
                cannon.decreaseAngle();
                break;
            case "SPACE": // shoot
                missiles.add(cannon.shoot());
                return true;
            case "F": // increase velocity
                GameConfig.VELOCITY += GameConfig.INCREASE_STEP;
                break;
            case "G": // decrease velocity
                if(GameConfig.VELOCITY > 0.5) {
                    GameConfig.VELOCITY -= GameConfig.INCREASE_STEP;
                }
                break;
            case "V": // increase gravity
                GameConfig.GRAVITY += GameConfig.INCREASE_STEP;
                break;
            case "B": // decrease gravity
                if(GameConfig.GRAVITY > 0) {
                    GameConfig.GRAVITY -= GameConfig.INCREASE_STEP;
                }
                break;
            default:
                //nothing
        }
        return false;
    }

    public void update() {
        checkCollisions();

        // Clear the canvas
        renderer.clearRect(0, 0, getWindowWidth(), getWindowHeight());

        renderer.drawText("Score: " + score, new Position(GameConfig.MAX_X - 100, 20));
        renderer.drawText("Angle: " + cannon.getAngle(), new Position(GameConfig.MAX_X - 100, 35));
        renderer.drawText("Velocity: " + GameConfig.VELOCITY, new Position(GameConfig.MAX_X - 100, 50));
        renderer.drawText("Gravity: " + GameConfig.GRAVITY, new Position(GameConfig.MAX_X - 100, 65));

        renderer.drawImage(cannon.texture, cannon.getPosition());
        enemies.forEach(enemy -> {
            renderer.drawImage(enemy.texture, enemy.getPosition());
        });
        missiles.forEach(missile -> {
            renderer.drawImage(missile.texture, missile.getPosition());
            missile.updatePosition();
        });
    }

    void checkCollisions() {
        missiles = missiles
                .stream()
                .filter(missile -> // out of board
                        missile.getX() >= 0 &&
                                missile.getX() < getWindowWidth() &&
                                missile.getY() < getWindowHeight()
                ).filter(missile -> { //collisions
                    var enemiesCount = enemies.size();
                    enemies = enemies.stream().filter(enemy -> {
                        var collided = missile.collideWith(enemy);
                        if(collided) ++score;
                        return !collided;
                    }).collect(Collectors.toList());
                    return enemiesCount <= enemies.size();
                })
                .collect(Collectors.toList());
    }

    public String getWindowTitle() {
        return GameConfig.GAME_TITLE;
    }

    public int getWindowWidth() {
        return GameConfig.MAX_X;
    }

    public int getWindowHeight() {
        return GameConfig.MAX_Y;
    }
}
