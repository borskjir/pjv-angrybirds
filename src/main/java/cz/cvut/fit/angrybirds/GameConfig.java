package cz.cvut.fit.angrybirds;

public class GameConfig {
    public static final int MAX_X = 1280;
    public static final int MAX_Y = 720;
    public static final int MOVE_STEP = 5;
    public static final double REAL_MOVE_TIME_INC = 0.05;
    public static double VELOCITY = 100.0;
    public static double GRAVITY = 10.0;
    public static final double INCREASE_STEP = 0.5;
    public static final String GAME_TITLE = "Angry Birds: Ultimate PJV Edition";
    public static final int ENEMIES_COUNT = 12;
}
