package cz.cvut.fit.angrybirds;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AngryBirds extends Application {

    private static final GameHandler gameHandler = new GameHandler();

    @Override
    public void init() {
        gameHandler.init();
    }

    @Override
    public void start(Stage stage) throws IOException {
        String winTitle = gameHandler.getWindowTitle();
        int winWidth = gameHandler.getWindowWidth();
        int winHeight = gameHandler.getWindowHeight();

        stage.setTitle(winTitle);

        Group root = new Group();
        Scene theScene = new Scene(root);
        stage.setScene(theScene);

        Canvas canvas = new Canvas(winWidth, winHeight);
        root.getChildren().add(canvas);

        gameHandler.setGameRenderer(new GameRenderer(canvas.getGraphicsContext2D()));

        // true as active (false for used)
        Map<String, Boolean> pressedKeysCodes = new HashMap<>();

        theScene.setOnKeyPressed(
                new EventHandler<KeyEvent>() {
                    public void handle(KeyEvent e) {
                        String code = e.getCode().toString();

                        // only add once... prevent duplicates
                        if (!pressedKeysCodes.containsKey(code))
                            pressedKeysCodes.putIfAbsent(code, false);
                    }
                }
        );

        theScene.setOnKeyReleased(
                new EventHandler<KeyEvent>() {
                    public void handle(KeyEvent e) {
                        String code = e.getCode().toString();
                        pressedKeysCodes.remove(code);
                    }
                }
        );

        // the game-loop
        new AnimationTimer() {
            public void handle(long currentNanoTime) {
                gameHandler.processPressedKeys(pressedKeysCodes);
                gameHandler.update();
            }
        }.start();

        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}