package cz.cvut.fit.angrybirds;

import java.util.Random;

public class Enemy extends GameObject{
    public Enemy() {
        if(Math.random() > 0.5) {
            texture = "src/main/resources/enemy1.png";
            height = 29;
            width = 30;
        } else {
            texture = "src/main/resources/enemy2.png";
            height = 26;
            width = 30;
        }
        setRandomPosition();
    }

    private void setRandomPosition() {
        Random random = new Random();

        setX(random.nextInt(GameConfig.MAX_X - 60) + 60);
        setY(random.nextInt(GameConfig.MAX_Y));
    }
}
