package cz.cvut.fit.angrybirds;

public class Position {
    private double dimX = 0;
    private double dimY = 0;

    public Position() {
    }

    public Position(Position pos2) {
        this.dimX = pos2.getX();
        this.dimY = pos2.getY();
    }

    public Position(double posX, double posY) {
        this.dimX = posX;
        this.dimY = posY;
    }

    public double getX() {
        return dimX;
    }

    public double getY() {
        return dimY;
    }

    public void setY(double y) {
        this.dimY = y;
    }

    public void setX(double x) {
        this.dimX = x;
    }

    public void move(double dx, double dy) {
        this.dimX += dx;
        this.dimY += dy;
    }
}
